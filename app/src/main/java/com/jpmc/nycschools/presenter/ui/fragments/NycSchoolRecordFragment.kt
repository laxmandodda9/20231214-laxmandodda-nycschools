package com.jpmc.nycschools.presenter.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jpmc.nycschools.R
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.databinding.FragmentSchoolScoresBinding
import com.jpmc.nycschools.presenter.ui.customviews.NoResultsView.Companion.SCHOOL_DATA_RETRY
import com.jpmc.nycschools.presenter.util.ActionListener
import com.jpmc.nycschools.presenter.util.BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS
import com.jpmc.nycschools.presenter.util.viewBinding
import com.jpmc.nycschools.presenter.viewModel.SchoolRecordViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NycSchoolRecordFragment : Fragment(R.layout.fragment_school_scores), ActionListener {
    private val binding by viewBinding(FragmentSchoolScoresBinding::bind)

    private val schoolViewModel: SchoolRecordViewModel by lazy {
        ViewModelProvider(this)[SchoolRecordViewModel::class.java]
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val nycSchoolInfoUiModal: NycSchoolInfoUiModal? = fetchDetails()
            initObserver()
            updateSchoolInfo(nycSchoolInfoUiModal)
        } ?: showErrorView()
    }

    private fun fetchDetails(): NycSchoolInfoUiModal? {
        val nycSchoolInfoUiModal: NycSchoolInfoUiModal? =
            arguments?.getParcelable(BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS)
        nycSchoolInfoUiModal?.let {
            binding.linearProgressBar.visibility = View.VISIBLE
            schoolViewModel.getSchoolSatScores(it.dbn)
        }
        return nycSchoolInfoUiModal
    }

    private fun initObserver() {
        schoolViewModel.schoolSatScoresLiveData.observe(viewLifecycleOwner) {
            it?.let {
                updateView(it)
            } ?: run {
                showErrorView()
            }
            binding.linearProgressBar.visibility = View.GONE
        }
    }

    /**
     * Method to show No Results view, when API Failures or something goes wrong
     */
    private fun showErrorView() = with(binding) {
        noResultscontainer.apply {
            visibility = View.VISIBLE
            actionListener = this@NycSchoolRecordFragment
        }
        resultsHeaderView.visibility = View.GONE
        satBodyData.visibility = View.GONE
    }


    private fun updateView(scoresUiModal: SatScoresUiModal) {
        with(binding) {
            noResultscontainer.visibility = View.GONE
            satBodyData.visibility = View.VISIBLE
            resultsHeaderView.visibility = View.VISIBLE
            satTestTakers.text = scoresUiModal.numOfSatTestTakers
            tvCriticalReading.text = context?.getString(
                R.string.critical_reading,
                scoresUiModal.satCriticalReadingAvgScore
            )
            tvMathCount.text =
                root.context.getString(R.string.maths_reading, scoresUiModal.satMathAvgScore)
            tvWritingCount.text =
                root.context.getString(R.string.writing, scoresUiModal.satWritingAvgScore)
        }
    }

    /**
     * Method to update School Contact Info phone Number , Website link
     * @param nycSchoolInfoUiModal : School Data
     */
    private fun updateSchoolInfo(nycSchoolInfoUiModal: NycSchoolInfoUiModal?) {
        with(binding) {
            tvOverViewInfo.text = nycSchoolInfoUiModal?.overViewInfo
            schoolName.text = nycSchoolInfoUiModal?.schoolName
            addressLine1.text = this.root.context.getString(
                R.string.school_details_address,
                nycSchoolInfoUiModal?.addressLine1,
                nycSchoolInfoUiModal?.addressLine2
            )
            contactUsContainer.setData(
                nycSchoolInfoUiModal?.phoneNumber.orEmpty(),
                nycSchoolInfoUiModal?.webSite.orEmpty()
            )
        }
    }

    override fun onAction(action: String, data: Any?) {
        when (action) {
            SCHOOL_DATA_RETRY -> fetchDetails()
        }
    }
}