package com.jpmc.nycschools.presenter.ui.customviews

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.jpmc.nycschools.databinding.ViewContactUsBinding


class ContactUsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private val binding = ViewContactUsBinding.inflate(LayoutInflater.from(context), this, true)

    fun setData(phoneNumber: String, url: String) {
        if (phoneNumber.isNotEmpty()) {
            binding.phoneButton.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:$phoneNumber")
                context.startActivity(intent)
            }
        } else {
            binding.phoneButton.visibility = View.GONE
        }

        if (url.isNotEmpty()) {
            binding.websiteButton.setOnClickListener {
                val website = if (url.contains("http")) {
                    url
                } else {
                    "https://$url"
                }
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(website))
                context.startActivity(intent)
            }
        } else {
            binding.websiteButton.visibility = View.GONE
        }
    }
}