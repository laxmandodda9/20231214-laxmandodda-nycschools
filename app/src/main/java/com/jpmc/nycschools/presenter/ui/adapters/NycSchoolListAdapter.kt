package com.jpmc.nycschools.presenter.ui.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.nycschools.R
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.databinding.ViewSchoolListItemBinding
import com.jpmc.nycschools.presenter.util.ActionListener

class NycSchoolListAdapter(private val actionListener: ActionListener) :
    ListAdapter<NycSchoolInfoUiModal, NycSchoolListAdapter.SchoolItemViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder {
        return SchoolItemViewHolder(
            ViewSchoolListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: SchoolItemViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }


    inner class SchoolItemViewHolder(private val binding: ViewSchoolListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(nycSchoolInfoUiModal: NycSchoolInfoUiModal?) {
            nycSchoolInfoUiModal?.let {
                with(binding) {
                    schoolName.text = it.schoolName
                    addressLine2.text = it.addressLine2
                    primaryAddressLine.text = it.addressLine1
                    totalStudents.text = root.context.getString(R.string.count_of_students, it.countOfStudents)
                    if (it.languages.isNotEmpty()) {
                        languages.text = it.languages
                        languages.visibility = View.VISIBLE
                        languagesLabels.visibility = View.VISIBLE
                    } else {
                        languages.visibility = View.GONE
                        languagesLabels.visibility = View.GONE
                    }
                    binding.root.setOnClickListener {
                        actionListener.onAction(ACTION_SCHOOL_SELECTED, nycSchoolInfoUiModal)
                    }
                }
            }
        }
    }

    internal companion object {
        const val ACTION_SCHOOL_SELECTED = "ACTION_SCHOOL_SELECTED"

        private val diffCallback: DiffUtil.ItemCallback<NycSchoolInfoUiModal> =
            object : DiffUtil.ItemCallback<NycSchoolInfoUiModal>() {
                override fun areItemsTheSame(
                    oldItem: NycSchoolInfoUiModal,
                    newItem: NycSchoolInfoUiModal
                ): Boolean {
                    return oldItem.dbn == newItem.dbn
                }

                override fun areContentsTheSame(
                    oldItem: NycSchoolInfoUiModal,
                    newItem: NycSchoolInfoUiModal
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }
}

