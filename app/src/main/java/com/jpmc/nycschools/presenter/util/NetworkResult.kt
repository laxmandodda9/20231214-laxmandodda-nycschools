package com.jpmc.nycschools.presenter.util

/**
 * Wrapper class to hold either a response or an error of an operation
 * Use [value] function to get the result of the operation
 * Use [error] function to get the error component
 */
sealed class NetworkResult<T> {
    open fun value(): T? = null
    open fun error(): Throwable? = null
}

/**
 * Represents a success state.
 * A non null object is returned when [value] is invoked
 */
data class Success<T>(val value: T) : NetworkResult<T>() {
    override fun value() = value
}

/**
 * Represents an error state.
 * Use to retrieve the error value of the operation
 */
data class Failure<T>(val failure: Throwable) : NetworkResult<T>() {
    override fun error(): Throwable? = failure
}

/**
 * A suspending function that runs the provided function block within [runCatching] and returns either
 * a [Success] or a [Failure] object
 *
 * Since the block is wrapped within the runCatching block the clients does not require any addition error
 * handling (try/catch blocks)
 *
 * @param block suspending block to be executed within [runCatching]
 * @return [Success] if the block executes successfully else [Failure] with the exception wrapped inside it
 */
suspend fun <R> safeCall(block: suspend () -> R): NetworkResult<R> {
    // runCatching catches exception and transforms the response to a Result
    val response = runCatching { block() }

    return when {
        response.isSuccess && response.getOrNull() == null -> Failure(EmptyBodyException())
        response.isSuccess -> Success(response.getOrNull()!!)
        else -> Failure(response.exceptionOrNull()!!)
    }
}

/**
 * Handle 204 Response code
 */
class EmptyBodyException(msg: String = "204 No Content", errorCode: Int = 204) : Exception(msg)