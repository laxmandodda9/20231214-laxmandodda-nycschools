package com.jpmc.nycschools.presenter.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.domain.usecase.SchoolUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolRecordViewModel @Inject constructor(private val schoolUseCase: SchoolUseCase) :
    ViewModel() {

    private val _schoolSatScoresMutableLiveData = MutableLiveData<SatScoresUiModal?>()
    val schoolSatScoresLiveData: LiveData<SatScoresUiModal?> = _schoolSatScoresMutableLiveData

    /**
     * Method to fetch School Scores Record
     * @param dbn - DBN
     */
    fun getSchoolSatScores(dbn: String) {
        viewModelScope.launch {
            val result = schoolUseCase.getSchoolSatScores(dbn)
            _schoolSatScoresMutableLiveData.value = result
        }
    }
}