package com.jpmc.nycschools.presenter.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.domain.usecase.SchoolUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val schoolUseCase: SchoolUseCase) : ViewModel() {

    private val _schoolsMutableLiveData = MutableLiveData<List<NycSchoolInfoUiModal>?>()
    val listOfSchoolsLiveData: LiveData<List<NycSchoolInfoUiModal>?> = _schoolsMutableLiveData

    /**
     * Method to fetch Nyc Schools List
     */
    fun fetchSchoolList() {
        viewModelScope.launch {
            val result = schoolUseCase.getSchoolsList()
            _schoolsMutableLiveData.value = result
        }
    }
}