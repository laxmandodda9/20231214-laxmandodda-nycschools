package com.jpmc.nycschools.presenter.util

val EMPTY = ""
val STR_COMMA : String = ","
val BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS = "BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS"