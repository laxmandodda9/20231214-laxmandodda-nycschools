package com.jpmc.nycschools.presenter.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jpmc.nycschools.databinding.ActivityMainBinding
import com.jpmc.nycschools.presenter.util.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}