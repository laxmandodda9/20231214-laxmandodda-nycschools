package com.jpmc.nycschools.presenter.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jpmc.nycschools.R
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.databinding.FragmentNycSchoolsBinding
import com.jpmc.nycschools.presenter.ui.adapters.NycSchoolListAdapter
import com.jpmc.nycschools.presenter.ui.customviews.NoResultsView.Companion.SCHOOL_DATA_RETRY
import com.jpmc.nycschools.presenter.util.ActionListener
import com.jpmc.nycschools.presenter.util.BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS
import com.jpmc.nycschools.presenter.util.viewBinding
import com.jpmc.nycschools.presenter.viewModel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NycSchoolsFragment : Fragment(R.layout.fragment_nyc_schools), ActionListener {

    private val binding by viewBinding(FragmentNycSchoolsBinding::bind)
    private val schoolViewModel: SchoolViewModel by lazy {
        ViewModelProvider(this)[SchoolViewModel::class.java]
    }
    private val nycSchoolListAdapter by lazy { NycSchoolListAdapter(this) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        pullToRefresh()
        initObserver()
        fetchNycSchoolsList()
    }

    /**
     * Method to initiate Observers
     */
    private fun initObserver() {
        schoolViewModel.listOfSchoolsLiveData.observe(viewLifecycleOwner) {
            binding.linearProgressBar.visibility = View.GONE
            it?.let {
                binding.swipeRefreshLayout.visibility = View.VISIBLE
                binding.noResultscontainerView.visibility = View.GONE
                nycSchoolListAdapter.submitList(it)
            } ?: run { displayErrorView() }

        }
    }

    /**
     * Method to display Error View
     */
    private fun displayErrorView() {
        with(binding) {
            noResultscontainerView.actionListener = this@NycSchoolsFragment
            swipeRefreshLayout.visibility = View.GONE
            noResultscontainerView.visibility = View.VISIBLE
        }
    }

    /**
     * Method to handle pull to Refresh
     */
    private fun pullToRefresh() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = false
            fetchNycSchoolsList()
        }
    }

    /**
     * Method to fetch Nyc Schools List
     */
    private fun fetchNycSchoolsList() {
        binding.linearProgressBar.visibility = View.VISIBLE
        schoolViewModel.fetchSchoolList()
    }

    /**
     * Method to setup SchoolList Adapter
     */
    private fun setupAdapter() {
        // Setup recycler view
        with(binding) {
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL
                )
            )
            recyclerView.adapter = nycSchoolListAdapter
        }
    }


    /**
     * Method to launch School Details Fragment with Selected School
     * @param nycSchoolInfoUiModal - Selected School Item
     */
    private fun launchSchoolInfoFragment(nycSchoolInfoUiModal: NycSchoolInfoUiModal) {
        findNavController().navigate(
            R.id.schoolDetailsFragment,
            bundleOf(BUNDLE_KEY_ARGUMENT_SCHOOL_DETAILS to nycSchoolInfoUiModal)
        )
    }

    override fun onAction(action: String, data: Any?) {
        when (action) {
            NycSchoolListAdapter.ACTION_SCHOOL_SELECTED -> launchSchoolInfoFragment(data as NycSchoolInfoUiModal)
            SCHOOL_DATA_RETRY -> fetchNycSchoolsList()
        }
    }
}