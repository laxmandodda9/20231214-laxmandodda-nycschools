package com.jpmc.nycschools.presenter.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.jpmc.nycschools.databinding.ViewNoResultsBinding
import com.jpmc.nycschools.presenter.util.ActionListener

class NoResultsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    var actionListener : ActionListener?= null
    private val binding = ViewNoResultsBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.retryBtn.setOnClickListener {
            actionListener?.onAction(SCHOOL_DATA_RETRY)
        }
    }

    companion object {
        const val SCHOOL_DATA_RETRY = "SCHOOL_DETAILS_RETRY"
    }
}