package com.jpmc.nycschools.presenter.util

interface ActionListener {
    fun onAction(action: String, data: Any? = null)
}