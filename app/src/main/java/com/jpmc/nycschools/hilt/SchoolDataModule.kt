package com.jpmc.nycschools.hilt

import com.jpmc.nycschools.data.repo.NycNycSchoolRepositoryImpl
import com.jpmc.nycschools.domain.repo.NycSchoolRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class SchoolDataModule {
    @Binds
    @ViewModelScoped
    abstract fun bindSchoolRepository(impl: NycNycSchoolRepositoryImpl): NycSchoolRepository
}