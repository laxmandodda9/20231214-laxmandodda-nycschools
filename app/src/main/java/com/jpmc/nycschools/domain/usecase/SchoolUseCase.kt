package com.jpmc.nycschools.domain.usecase

import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.domain.repo.NycSchoolRepository
import javax.inject.Inject

class SchoolUseCase @Inject constructor(private val repository: NycSchoolRepository) {
    suspend fun getSchoolsList(): List<NycSchoolInfoUiModal>? =
        repository.fetchListOfSchools()


    suspend fun getSchoolSatScores(dbn: String) =
        repository.getSchoolSatScores(dbn)

}