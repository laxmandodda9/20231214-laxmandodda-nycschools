package com.jpmc.nycschools.domain.repo

import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal

interface NycSchoolRepository {
    suspend  fun fetchListOfSchools() : List<NycSchoolInfoUiModal>?

    suspend fun getSchoolSatScores(dbn : String) : SatScoresUiModal?
}