package com.jpmc.nycschools.data.repo

import com.jpmc.nycschools.data.mapper.NycSchoolListMapper
import com.jpmc.nycschools.data.mapper.NycSchoolSatScoresMapper
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.data.source.NycSchoolDataSource
import com.jpmc.nycschools.domain.repo.NycSchoolRepository
import javax.inject.Inject

class NycNycSchoolRepositoryImpl @Inject constructor(
    private val nycSchoolDataSource: NycSchoolDataSource,
    private val mapper: NycSchoolListMapper,
    private val satScoresMapper: NycSchoolSatScoresMapper
) : NycSchoolRepository {

    override suspend fun fetchListOfSchools(): MutableList<NycSchoolInfoUiModal>? {
        val result = nycSchoolDataSource.getListOfSchools()
        return mapper.mapToEntityFromDto(result.value())
    }

    override suspend fun getSchoolSatScores(dbn: String): SatScoresUiModal? {
        val result = nycSchoolDataSource.getSchoolSatScores(dbn)
        return satScoresMapper.mapToEntityFromDto(result.value())
    }
}