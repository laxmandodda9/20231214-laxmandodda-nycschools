package com.jpmc.nycschools.data.mapper

import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.domain.model.SatScores
import javax.inject.Inject

class NycSchoolSatScoresMapper @Inject constructor() :
    BaseMapper<SatScores, SatScoresUiModal> {
    override fun mapToEntityFromDto(data: SatScores?): SatScoresUiModal? {
        data?.let {
            return SatScoresUiModal(
                schoolName = data.schoolName.orEmpty(),
                numOfSatTestTakers = data.numOfSatTestTakers.orEmpty(),
                satCriticalReadingAvgScore = data.satCriticalReadingAvgScore.orEmpty(),
                satMathAvgScore = data.satMathAvgScore.toString(),
                satWritingAvgScore = data.satWritingAvgScore.toString()
            )
        } ?: return null
    }
}