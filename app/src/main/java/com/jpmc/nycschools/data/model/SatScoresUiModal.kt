package com.jpmc.nycschools.data.model

data class SatScoresUiModal(
    val schoolName: String,
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String
)
