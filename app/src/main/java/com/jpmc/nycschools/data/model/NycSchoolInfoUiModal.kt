package com.jpmc.nycschools.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NycSchoolInfoUiModal(
    var dbn: String,
    var schoolName: String,
    val countOfStudents: String,
    val addressLine1: String,
    val addressLine2: String,
    val phoneNumber : String,
    val webSite : String,
    val email: String? = null,
    val overViewInfo : String,
    val languages: String
) : Parcelable
