package com.jpmc.nycschools.data.mapper

import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.domain.model.SchoolResponse
import com.jpmc.nycschools.presenter.util.STR_COMMA
import javax.inject.Inject

class NycSchoolListMapper @Inject constructor() :
    BaseMapper<List<SchoolResponse>, List<NycSchoolInfoUiModal>> {
    override fun mapToEntityFromDto(data: List<SchoolResponse>?): MutableList<NycSchoolInfoUiModal>? {
        val nycSchoolInfoUiModalList = mutableListOf<NycSchoolInfoUiModal>()
        data?.forEach {
            val nycSchoolInfoUiModal = NycSchoolInfoUiModal(
                dbn = it.dbn,
                schoolName = it.school_name,
                countOfStudents = it.totalStudents,
                addressLine1 = it.primary_address_line_1 + STR_COMMA,
                addressLine2 = it.city + STR_COMMA + it.state_code + STR_COMMA + it.zip,
                phoneNumber = it.phone_number,
                webSite = it.website,
                email = it.school_email,
                overViewInfo = it.overview_paragraph,
                languages = it.languageClasses
            )
            nycSchoolInfoUiModalList.add(nycSchoolInfoUiModal)
        } ?: return null
        return nycSchoolInfoUiModalList
    }
}