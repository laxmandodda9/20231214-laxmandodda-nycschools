package com.jpmc.nycschools.data.mapper

interface BaseMapper<Dto, Entity> {
    fun mapToEntityFromDto(data: Dto?): Entity?
}