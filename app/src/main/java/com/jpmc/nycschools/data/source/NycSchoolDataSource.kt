package com.jpmc.nycschools.data.source

import com.jpmc.nycschools.presenter.util.safeCall
import javax.inject.Inject

class NycSchoolDataSource @Inject constructor(private val service: NycSchoolService) {

    suspend fun getListOfSchools() = safeCall {
        service.fetchListOfNycSchools()
    }

    suspend fun getSchoolSatScores(dbn: String) =
        safeCall { service.fetchSchoolData(dbn)?.firstOrNull() }
}