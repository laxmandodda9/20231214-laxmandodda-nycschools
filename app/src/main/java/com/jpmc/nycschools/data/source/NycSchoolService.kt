package com.jpmc.nycschools.data.source

import com.jpmc.nycschools.domain.model.SatScores
import com.jpmc.nycschools.domain.model.SchoolResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface NycSchoolService  {
    @GET("/resource/s3k6-pzi2.json")
    suspend fun fetchListOfNycSchools(): List<SchoolResponse>?

    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSchoolData(@Query("dbn") dbn :String) : List<SatScores>?
}