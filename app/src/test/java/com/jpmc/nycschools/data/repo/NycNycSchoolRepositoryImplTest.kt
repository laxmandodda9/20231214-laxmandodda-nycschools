package com.jpmc.nycschools.data.repo

import com.jpmc.nycschools.BaseTest
import com.jpmc.nycschools.data.mapper.NycSchoolListMapper
import com.jpmc.nycschools.data.mapper.NycSchoolSatScoresMapper
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.data.source.NycSchoolDataSource
import com.jpmc.nycschools.domain.model.SatScores
import com.jpmc.nycschools.domain.model.SchoolResponse
import com.jpmc.nycschools.presenter.util.safeCall
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertNull
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

internal class NycNycSchoolRepositoryImplTest : BaseTest() {
    @Mock
    private lateinit var schoolService: NycSchoolDataSource

    @Mock
    private lateinit var satScoresMapper: NycSchoolSatScoresMapper

    @Mock
    private lateinit var nycSchoolListMapper: NycSchoolListMapper

    private lateinit var repository: NycNycSchoolRepositoryImpl

    @Before
    override fun setup() {
        super.setup()
        repository = NycNycSchoolRepositoryImpl(schoolService, nycSchoolListMapper, satScoresMapper)
    }

    @Test
    fun testGetListOfSchool_Success() = runTest {
        val schoolResponseMutableList = mutableListOf<SchoolResponse>()
        schoolResponseMutableList.add(
            SchoolResponse(
                school_name = "Liberation Diploma",
                dbn = DBN
            )
        )

        Mockito.`when`(schoolService.getListOfSchools()).thenReturn(
            safeCall { schoolResponseMutableList }
        )

        val result = repository.fetchListOfSchools()
        assertNotNull(result)
    }

    @Test
    fun testGetListOfSchool_Failure() = runTest {
        Mockito.`when`(schoolService.getListOfSchools()).thenReturn(
            safeCall { null }
        )

        val result = repository.fetchListOfSchools()
        assertNotNull(result)
        assertTrue(result?.isEmpty() == true)
    }


    @Test
    fun testGetSchoolSatScores_Success() = runTest {
        val satScores = Mockito.mock(SatScores::class.java)
        val satScoresUiModal = Mockito.mock(SatScoresUiModal::class.java)
        Mockito.`when`(satScoresMapper.mapToEntityFromDto(satScores)).thenReturn(
            satScoresUiModal
        )
        Mockito.`when`(schoolService.getSchoolSatScores(DBN)).thenReturn(
            safeCall { satScores }
        )
        val result = repository.getSchoolSatScores(DBN)
        assertNotNull(result)
    }

    @Test
    fun testGetSchoolSatScores_Failure() = runTest {
        Mockito.`when`(schoolService.getSchoolSatScores(DBN)).thenReturn(
            safeCall { null }
        )
        val result = repository.getSchoolSatScores(DBN)
        assertNull(result)
    }

    private companion object {
        private const val DBN = "21K745"
    }
}