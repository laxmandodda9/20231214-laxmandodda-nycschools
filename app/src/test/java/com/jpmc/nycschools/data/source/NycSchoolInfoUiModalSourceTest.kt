package com.jpmc.nycschools.data.source

import com.jpmc.nycschools.BaseTest
import com.jpmc.nycschools.domain.model.SatScores
import com.jpmc.nycschools.domain.model.SchoolResponse
import junit.framework.Assert
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

internal class NycSchoolInfoUiModalSourceTest : BaseTest() {
    @Mock
    private lateinit var nycSchoolService: NycSchoolService

    private lateinit var nycSchoolDataSource: NycSchoolDataSource

    @Before
    override fun setup() {
        super.setup()
        nycSchoolDataSource = NycSchoolDataSource(nycSchoolService)
    }


    @Test
    fun testGetListOfSchool_Success() = runTest {
        val schoolResponseMutableList = mutableListOf<SchoolResponse>()
        schoolResponseMutableList.add(
            SchoolResponse(
                school_name = "Liberation Diploma",
                dbn = DBN
            )
        )
        Mockito.`when`(nycSchoolService.fetchListOfNycSchools()).thenReturn(
            schoolResponseMutableList
        )
        val result = nycSchoolDataSource.getListOfSchools()
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.value()?.size)
    }


    @Test
    fun testGetListOfSchool_Failure() = runTest {
        Mockito.`when`(nycSchoolService.fetchListOfNycSchools()).thenReturn(
           null
        )
        val result = nycSchoolDataSource.getListOfSchools()
        Assert.assertNull(result.value())
    }


    @Test
    fun testGetSchoolSatScores_Success() = runTest {
        val satScores = Mockito.mock(SatScores::class.java)
        val listOfSatScores = listOf<SatScores>(satScores)
        Mockito.`when`(nycSchoolService.fetchSchoolData(DBN)).thenReturn(
            listOfSatScores
        )
        val result = nycSchoolDataSource.getSchoolSatScores(DBN)
        Assert.assertNotNull(result)
    }

    @Test
    fun testGetSchoolSatScores_Failure() = runTest {
        Mockito.`when`(nycSchoolService.fetchSchoolData(DBN)).thenReturn(
            null
        )
        val result = nycSchoolDataSource.getSchoolSatScores(DBN)
        Assert.assertNull(result.value())
    }

    private companion object {
        private const val DBN = "21K745"
    }
}