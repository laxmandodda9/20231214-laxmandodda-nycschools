package com.jpmc.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.LiveData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import java.util.concurrent.CountDownLatch

open class BaseViewModelTest : BaseTest() {
    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockLifecycleOwner: LifecycleOwner

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @Before
    override fun setup() {
        super.setup()
        Mockito.`when`(mockLifecycleOwner.lifecycle).thenReturn(
            LifecycleRegistry(mockLifecycleOwner).apply { handleLifecycleEvent(Lifecycle.Event.ON_RESUME) })
    }

    fun <T> getLiveData(liveData: LiveData<T>, liveDataAPI: () -> Unit) {
        val countDownLatch = CountDownLatch(1)
        liveData.observe(mockLifecycleOwner, androidx.lifecycle.Observer { t ->
            countDownLatch.countDown()
        })
        liveDataAPI.invoke()
        countDownLatch.await()
    }
}