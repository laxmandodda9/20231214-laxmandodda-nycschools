package com.jpmc.nycschools.presenter.viewModel

import com.jpmc.nycschools.BaseViewModelTest
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.domain.usecase.SchoolUseCase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito


class SchoolViewModelTest : BaseViewModelTest() {
    @Mock
    private lateinit var schoolUseCase: SchoolUseCase

    private lateinit var viewModel: SchoolViewModel

    @Before
    override fun setup() {
        super.setup()
        viewModel = SchoolViewModel(schoolUseCase)
    }

    @Test
    fun testGetListOfSchools_success() = runTest {
        val schoolsList = mutableListOf<NycSchoolInfoUiModal>()
        schoolsList.add(
            NycSchoolInfoUiModal(
                schoolName = "Liberation Diploma",
                dbn = "21K745", email = "school@gmail.com",
                phoneNumber = "1234567890", webSite = "www.google.com",
                addressLine2 = "Edison,08887", addressLine1 = "NewJerssy", countOfStudents = "246",
                overViewInfo = "The mission of Liberation Diploma Plus High School",
                languages = "French, Spanish"
            )
        )

        Mockito.`when`(schoolUseCase.getSchoolsList()).thenReturn(
            schoolsList
        )
        getLiveData(viewModel.listOfSchoolsLiveData) {
            viewModel.fetchSchoolList(

            )
        }
        val result = viewModel.listOfSchoolsLiveData.value
        assertNotNull(result)
        assertEquals(1, result?.size)
        assertEquals(DBN, result?.first()?.dbn)
    }

    @Test
    fun testGetListOfSchools_withFailure() = runTest {
        val schoolsList = emptyList<NycSchoolInfoUiModal>()

        Mockito.`when`(schoolUseCase.getSchoolsList()).thenReturn(
            schoolsList
        )
        getLiveData(viewModel.listOfSchoolsLiveData) {
            viewModel.fetchSchoolList(
            )
        }
        val result = viewModel.listOfSchoolsLiveData.value
        assertNotNull(result)
        assertTrue(result?.isEmpty() == true)
    }

    private companion object {
        private const val DBN = "21K745"
    }
}