package com.jpmc.nycschools.presenter.viewModel

import com.jpmc.nycschools.BaseViewModelTest
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.domain.usecase.SchoolUseCase
import junit.framework.Assert
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class SchoolRecordViewModelTest : BaseViewModelTest() {
    @Mock
    private lateinit var schoolUseCase: SchoolUseCase

    private lateinit var viewModel: SchoolRecordViewModel

    @Before
    override fun setup() {
        super.setup()
        viewModel = SchoolRecordViewModel(schoolUseCase)
    }

    @Test
    fun testGetSchoolSatScores_success() = runTest {
        val satScoresUiModal = Mockito.mock(SatScoresUiModal::class.java)
        Mockito.`when`(schoolUseCase.getSchoolSatScores(DBN)).thenReturn(
            satScoresUiModal
        )
        getLiveData(viewModel.schoolSatScoresLiveData) {
            viewModel.getSchoolSatScores(DBN)
        }
        val result = viewModel.schoolSatScoresLiveData.value
        Assert.assertNotNull(result)
    }

    @Test
    fun testGetSchoolSatScores_failure() = runTest {
        Mockito.`when`(schoolUseCase.getSchoolSatScores(DBN)).thenReturn(
            null
        )
        getLiveData(viewModel.schoolSatScoresLiveData) {
            viewModel.getSchoolSatScores(DBN)
        }
        val result = viewModel.schoolSatScoresLiveData.value
        Assert.assertNull(result)
    }

    private companion object {
        private const val DBN = "21K745"
    }
}