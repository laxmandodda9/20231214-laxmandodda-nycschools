package com.jpmc.nycschools.domain.usecase

import com.jpmc.nycschools.BaseTest
import com.jpmc.nycschools.data.model.SatScoresUiModal
import com.jpmc.nycschools.data.model.NycSchoolInfoUiModal
import com.jpmc.nycschools.domain.repo.NycSchoolRepository
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

internal class SchoolUseCaseTest : BaseTest() {
    @Mock
    private lateinit var repository: NycSchoolRepository
    private lateinit var useCase: SchoolUseCase

    @Before
    override fun setup() {
        super.setup()
        useCase = SchoolUseCase(repository)
    }

    @Test
    fun testGetSchoolsList_success() =
        runTest {
            val schoolsList = mutableListOf<NycSchoolInfoUiModal>()
            schoolsList.add(
                NycSchoolInfoUiModal(
                    schoolName = "Liberation Diploma",
                    dbn = "21K745",
                    email = "school@gmail.com",
                    phoneNumber = "1234567890",
                    webSite = "www.google.com",
                    addressLine2 = "Edison,08887",
                    addressLine1 = "NewJerssy",
                    countOfStudents = "246",
                    overViewInfo = "The mission of Liberation Diploma Plus High School",
                    languages = "French, Spanish"
                )
            )

            Mockito.`when`(repository.fetchListOfSchools()).thenReturn(
                schoolsList
            )
            val result = useCase.getSchoolsList()
            Assert.assertNotNull(result)
        }

    @Test
    fun testGetSchoolSatScores_success() = runTest {
        val satScoresUiModal = Mockito.mock(SatScoresUiModal::class.java)
        Mockito.`when`(repository.getSchoolSatScores(DBN)).thenReturn(
            satScoresUiModal
        )
        val result = useCase.getSchoolSatScores(DBN)
        Assert.assertNotNull(result)
    }


    private companion object {
        private const val DBN = "21K745"
    }
}