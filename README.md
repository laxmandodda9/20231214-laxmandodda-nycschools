# NYCSchools

# Main Features
- Display list of NYC High Schools
- Display all the SAT Scores - including Math, Reading, Writing with OverView
- No Results Screen

# Environement Settings
- Android Studio Giraffe or above
- Java 17

## Architecture
- Clean Architecture with MVVM (Model - ComposableView - ViewModel)
- Repository pattern
- Hilt - dependency injection

## Clean Architecture
Clean Architecture basically focuses on the separation of concerns. 
There is no specified number of layers while following the Clean Architecture.
The rule of thumb is that the inner layer should not refer to any of the outer layers and as we go toward the center, the details should become abstract.
![Architecture.png](Architecture.png)

## Built With 🛠
- [Kotlin](https://kotlinlang.org/) - First class and official programming language for Android development.
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) - For asynchronous and more..
- [Dependency Injection](https://developer.android.com/training/dependency-injection)
    - [Hilt](https://dagger.dev/hilt) - Easier way to incorporate Dagger DI into Android apps.
- [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.

### App Screenshot
![Loading.png](Loading.png)
![HomeScreeen.png](HomeScreeen.png)
![Details.png](Details.png)
![ListScreenErrorView.png](ListScreenErrorView.png)
![NoResults.png](NoResults.png)


### PullToRefresh
Implementing a pull-to-refresh functionality in Android can be achieved using the SwipeRefreshLayout widget, which is part of the Android Support Library (or AndroidX library). 
This widget allows users to trigger a refresh by swiping down on the screen.
![PullToRefresh.png](PullToRefresh.png)

### UnitTests
JUnit is a widely-used testing framework for Java, and it is commonly used for testing Android applications as well. 
JUnit provides annotations to identify methods that specify a test, and it provides assertion methods to verify expected outcomes.
![UnitTests.png](UnitTests.png)